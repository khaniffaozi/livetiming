<div class="header">
    <nav class="navbar nav-menu mainNav affix-top">
        <div class="">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed main-menu-open" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="ico-burger-menu"></span>
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="#" class="header-logo" onclick="CallEventsWithCleardSearchTerm();">
                    <img src="https://speedhive.mylaps.com/Images/images/mylaps_speedhive_logo.png" alt="MyLaps" height="50" width="432">
                </a>
                <div class="left-arrow-div">
                    <a class="ico ico-arrow-left-gray" href="" onclick="history.go(-1); return false;"></a>
                </div>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav nav-tabs nav-menu-inner">
                    <li role="presentation">
                        <a href="<?php echo  base_url(); ?>" id="search-result-layout">
                            <p class="nav-elem">ALL EVENT RESULTS</p>
                        </a>
                        <span class="ico ico-nav-active"></span>
                    </li>
                    <li role="presentation">
                        <a href="<?php echo base_url();?>livetiming">
                            <p class="nav-elem" style="text-transform: uppercase;">Live Timing</p>
                        </a>
                        <span class="ico ico-nav-active"></span>
                    </li>
                    <li role="presentation" class="upMenu">
                        <a href="https://www.mylaps.com" target="_blank">MYLAPS.COM</a>
                    </li>
                    <li role="presentation" class="upMenu">
                        <a href="/app">DOWNLOAD APP</a>
                    </li>
                    <li role="presentation" class="upMenu">
                        <a href="https://speedhiveshop.mylaps.com" target="_blank">SHOP</a>
                    </li>
                    <li role="presentation" class="upMenu">
                      <a href="https://speedhiveblog.mylaps.com/" target="_blank">BLOG</a>
                    </li>
                    <li role="presentation" class="upMenu">
                        <a href="/x2link">X2 LINK</a>
                    </li>
                    <li id="language-change" role="presentation" class="upMenu">
                        <a class="english" href="#" data-language="en" style="color: white;">ENGLISH</a>
                        <a class="dutch" href="#" data-language="nl">NEDERLANDS</a>
                        <a class="japan" href="#" data-language="ja">日本語</a>
                    </li>
                        <li role="presentation" class="upMenu">
                            <div class="links-container">
                                <div class="links-first-row clearfix">
                                    <a href="/account/RegisterUserFirstStep" class="btn btn-signup">SIGNUP</a>
                                    <a href="/account/loginUser" class="btn btn-login">LOGIN</a>
                                </div>
                                <div class="links-second-row social-link clearfix">
								    <div class="row">

								            <a class="social ico-facebook" href="https://www.facebook.com/mylapsspeedhive"></a>
								            <a class="social ico-yahoo" href="https://www.youtube.com/channel/UCgd-KMnDXno6s_SHN_w1GxQ"></a>
								    </div>

                                </div>
                            </div>
                        </li>
                </ul><!-- nav-menu-inner -->
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
</div>