<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<html>
	<!-- <head> -->
	<head prefix="og:http://ogp.me/ns#">
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">


		<meta name="apple-itunes-app" content="app-id=988412583" app-argument="">
		<meta name="google-play-app" content="app-id=com.mylaps.speedhive">
		<title>RACE 9. MATIC 115CC PEMULA, SUPER NIGHT ROAD RACE SERI 1 2018, Noname Indonesia, Indonesia - Official Event Results, MYLAPS Speedhive</title>
		<meta property="og:title" content="RACE 9. MATIC 115CC PEMULA, SUPER NIGHT ROAD RACE SERI 1 2018, Noname Indonesia, Indonesia - Official Event Results, MYLAPS Speedhive">
		<meta name="description" content="Explore official results, positions, times, best laps, lapcharts, comparison and more data, SUPER NIGHT ROAD RACE SERI 1 2018, race event 03 March 2018 12:00">
		<meta name="og:description" content="Explore official results, positions, times, best laps, lapcharts, comparison and more data, SUPER NIGHT ROAD RACE SERI 1 2018, race event 03 March 2018 12:00">
		<meta property="og:image" content="https://speedhive.mylaps.com/Images/images/share.jpg">
		<meta name="twitter:card" content="summary_large_image">
		<meta name="twitter:site" content="@mylaps">
		<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
		<link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
		<link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
		<link rel="manifest" href="/manifest.json">
		<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#f7941e">
		<link rel="shortcut icon" href="/favicon.ico">
		<meta name="msapplication-TileColor" content="#f7941e">
		<meta name="msapplication-TileImage" content="/mstile-144x144.png">
		<meta name="msapplication-config" content="/browserconfig.xml">
		<meta name="theme-color" content="#ffffff">

		<link href="https://speedhive.azureedge.net/Content/css?1.0.0.25664" rel="stylesheet">
		<script type="text/javascript" async="" src="https://www.google-analytics.com/analytics.js"></script><script src="https://connect.facebook.net/signals/config/872821369500712?v=2.8.12&amp;r=stable" async=""></script><script async="" src="//connect.facebook.net/en_US/fbevents.js"></script><script type="text/javascript" async="" src="//www.googleadservices.com/pagead/conversion_async.js"></script><script async="" src="https://www.google-analytics.com/analytics.js"></script><script async="" src="//www.googletagmanager.com/gtm.js?id=GTM-N3RH8Z"></script><script id="twitter-wjs" src="https://platform.twitter.com/widgets.js"></script><script id="facebook-jssdk" src="//connect.facebook.net/en_US/sdk.js"></script><script>(function() {
		            var loadFallback,
		                len = document.styleSheets.length;
		            for (var i = 0; i < len; i++) {
		                var sheet = document.styleSheets[i];
		                if (sheet.href.indexOf('https://speedhive.azureedge.net/Content/css?1.0.0.25664') !== -1) {
		                    var meta = document.createElement('meta');
		                    meta.className = 'sr-only';
		                    document.head.appendChild(meta);
		                    var value = window.getComputedStyle(meta).getPropertyValue('width');
		                    document.head.removeChild(meta);
		                    if (value !== '1px') {
		                        if(document.readyState === 'complete') {
		                            var head  = document.getElementsByTagName('head')[0];
		                            var link  = document.createElement('link');
		                            link.rel  = 'stylesheet';
		                            link.type = 'text/css';
		                            link.href = '/Content/css?1.0.0.25664';
		                            head.appendChild(link);
		                            return true;
		                        } else {
		                            document.write('<link href="/Content/css?1.0.0.25664" rel="stylesheet" type="text/css" />');
		                        }
		                    }
		                }
		            }
		            return true;
		        }())||document.write('<script src="/Content/css"><\/script>');</script><style>@media print {#ghostery-purple-box {display:none !important}}</style>


		<link href="https://speedhive.azureedge.net/Content/session?1.0.0.25664" rel="stylesheet">
		<script>(function() {
		            var loadFallback,
		                len = document.styleSheets.length;
		            for (var i = 0; i < len; i++) {
		                var sheet = document.styleSheets[i];
		                if (sheet.href.indexOf('https://speedhive.azureedge.net/Content/session?1.0.0.25664') !== -1) {
		                    var meta = document.createElement('meta');
		                    meta.className = 'lapchart lapchart-header';
		                    document.head.appendChild(meta);
		                    var value = window.getComputedStyle(meta).getPropertyValue('height');
		                    document.head.removeChild(meta);
		                    if (value !== '50px') {
		                        if(document.readyState === 'complete') {
		                            var head  = document.getElementsByTagName('head')[0];
		                            var link  = document.createElement('link');
		                            link.rel  = 'stylesheet';
		                            link.type = 'text/css';
		                            link.href = '/Content/session?1.0.0.25664';
		                            head.appendChild(link);
		                            return true;
		                        } else {
		                            document.write('<link href="/Content/session?1.0.0.25664" rel="stylesheet" type="text/css" />');
		                        }
		                    }
		                }
		            }
		            return true;
		        }())||document.write('<script src="/Content/session"><\/script>');</script>


		<script src="https://speedhive.azureedge.net/bundles/scripts?1.0.0.25664"></script>
		<script>(window.$)||document.write('<script src="/bundles/scripts"><\/script>');</script>

		<link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,300i,700" rel="stylesheet">
		<style>
			.header {
			    min-height: 152px;
			    padding: 14px 0 0;
			}
		</style>

	</head>
	<body class="page-sessions page-sessions-details">
		<div class="page-container">
			<?php $this->load->view('header');?>
			<div class="body-content" id="js_print">
				<div class="container">
					<div class="sticky-header-global">
				        <div class="sticky-header-global-text"></div>
				    </div>
					<div class="row affix-top">
						<div class="col-xs-12">
							<?php echo $result1; ?>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<ul id="session-options" class="session-options-tabs clearfix" role="tablist">                  
								<li role="presentation" class="active">                      
									<a href="#all-results" aria-controls="all-results" class="event-tab allresults-tab" role="tab" data-toggle="tab">All Results</a>                  
								</li>   
							</ul>
						</div>
					</div>
					<?php echo $result2; ?>
				</div>
			</div>
		</div>
	</body>
</html>