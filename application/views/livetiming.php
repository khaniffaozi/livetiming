<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<html>
	<!-- <head> -->
	<head prefix="og:http://ogp.me/ns#">
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
		  

		<meta name="apple-itunes-app" content="app-id=988412583" app-argument="">
		<meta name="google-play-app" content="app-id=com.mylaps.speedhive">
		<title>LiveTiming</title>
		<meta property="og:title" content="MYLAPS Speedhive">
		<meta name="description" content="The platform for racers. MYLAPS Speedhive is your place to check results, follow races and go faster.">
		<meta name="og:description" content="The platform for racers. MYLAPS Speedhive is your place to check results, follow races and go faster.">
		<meta property="og:image" content="https://speedhive.mylaps.com/Images/images/share.jpg">
		<meta name="twitter:card" content="summary_large_image">
		<meta name="twitter:site" content="@mylaps">
		<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
		<link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
		<link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
		<link rel="manifest" href="/manifest.json">
		<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#f7941e">
		<link rel="shortcut icon" href="/favicon.ico">
		<meta name="msapplication-TileColor" content="#f7941e">
		<meta name="msapplication-TileImage" content="/mstile-144x144.png">
		<meta name="msapplication-config" content="/browserconfig.xml">
		<meta name="theme-color" content="#ffffff">
		  
		  <link href="https://speedhive.azureedge.net/Content/css?1.0.0.25664" rel="stylesheet">
		<script type="text/javascript" async="" src="https://www.google-analytics.com/analytics.js"></script><script async="" src="//connect.facebook.net/en_US/fbevents.js"></script><script type="text/javascript" async="" src="//www.googleadservices.com/pagead/conversion_async.js"></script><script async="" src="https://www.google-analytics.com/analytics.js"></script><script async="" src="//www.googletagmanager.com/gtm.js?id=GTM-N3RH8Z"></script><script>(function() {
		                var loadFallback,
		                    len = document.styleSheets.length;
		                for (var i = 0; i < len; i++) {
		                    var sheet = document.styleSheets[i];
		                    if (sheet.href.indexOf('https://speedhive.azureedge.net/Content/css?1.0.0.25664') !== -1) {
		                        var meta = document.createElement('meta');
		                        meta.className = 'sr-only';
		                        document.head.appendChild(meta);
		                        var value = window.getComputedStyle(meta).getPropertyValue('width');
		                        document.head.removeChild(meta);
		                        if (value !== '1px') {
		                            if(document.readyState === 'complete') {
		                                var head  = document.getElementsByTagName('head')[0];
		                                var link  = document.createElement('link');
		                                link.rel  = 'stylesheet';
		                                link.type = 'text/css';
		                                link.href = '/Content/css?1.0.0.25664';
		                                head.appendChild(link);
		                                return true;
		                            } else {
		                                document.write('<link href="/Content/css?1.0.0.25664" rel="stylesheet" type="text/css" />');
		                            }
		                        }
		                    }
		                }
		                return true;
		            }())||document.write('<script src="/Content/css"><\/script>');</script>

		  
		    <link href="https://speedhive.azureedge.net/Content/flags?1.0.0.25664" rel="stylesheet">
		<script>(function() {
		                var loadFallback,
		                    len = document.styleSheets.length;
		                for (var i = 0; i < len; i++) {
		                    var sheet = document.styleSheets[i];
		                    if (sheet.href.indexOf('https://speedhive.azureedge.net/Content/flags?1.0.0.25664') !== -1) {
		                        var meta = document.createElement('meta');
		                        meta.className = 'ico-flag-ad';
		                        document.head.appendChild(meta);
		                        var value = window.getComputedStyle(meta).getPropertyValue('background-position');
		                        document.head.removeChild(meta);
		                        if (value !== '0 0') {
		                            if(document.readyState === 'complete') {
		                                var head  = document.getElementsByTagName('head')[0];
		                                var link  = document.createElement('link');
		                                link.rel  = 'stylesheet';
		                                link.type = 'text/css';
		                                link.href = '/Content/flags?1.0.0.25664';
		                                head.appendChild(link);
		                                return true;
		                            } else {
		                                document.write('<link href="/Content/flags?1.0.0.25664" rel="stylesheet" type="text/css" />');
		                            }
		                        }
		                    }
		                }
		                return true;
		            }())||document.write('<script src="/Content/flags"><\/script>');</script><link href="/Content/flags?1.0.0.25664" rel="stylesheet" type="text/css">

		    <link href="https://speedhive.azureedge.net/Content/livetiming?1.0.0.25664" rel="stylesheet">
		<script>(function() {
		                var loadFallback,
		                    len = document.styleSheets.length;
		                for (var i = 0; i < len; i++) {
		                    var sheet = document.styleSheets[i];
		                    if (sheet.href.indexOf('https://speedhive.azureedge.net/Content/livetiming?1.0.0.25664') !== -1) {
		                        var meta = document.createElement('meta');
		                        meta.className = 'green-flag';
		                        document.head.appendChild(meta);
		                        var value = window.getComputedStyle(meta).getPropertyValue('width');
		                        document.head.removeChild(meta);
		                        if (value !== '30px') {
		                            if(document.readyState === 'complete') {
		                                var head  = document.getElementsByTagName('head')[0];
		                                var link  = document.createElement('link');
		                                link.rel  = 'stylesheet';
		                                link.type = 'text/css';
		                                link.href = '/Content/livetiming?1.0.0.25664';
		                                head.appendChild(link);
		                                return true;
		                            } else {
		                                document.write('<link href="/Content/livetiming?1.0.0.25664" rel="stylesheet" type="text/css" />');
		                            }
		                        }
		                    }
		                }
		                return true;
		            }())||document.write('<script src="/Content/livetiming"><\/script>');</script>

		    <link href="https://speedhive.azureedge.net/Content/livetiming-events?1.0.0.25664" rel="stylesheet">
		<script>(function() {
		                var loadFallback,
		                    len = document.styleSheets.length;
		                for (var i = 0; i < len; i++) {
		                    var sheet = document.styleSheets[i];
		                    if (sheet.href.indexOf('https://speedhive.azureedge.net/Content/livetiming-events?1.0.0.25664') !== -1) {
		                        var meta = document.createElement('meta');
		                        meta.className = 'green-flag';
		                        document.head.appendChild(meta);
		                        var value = window.getComputedStyle(meta).getPropertyValue('width');
		                        document.head.removeChild(meta);
		                        if (value !== '30px') {
		                            if(document.readyState === 'complete') {
		                                var head  = document.getElementsByTagName('head')[0];
		                                var link  = document.createElement('link');
		                                link.rel  = 'stylesheet';
		                                link.type = 'text/css';
		                                link.href = '/Content/livetiming-events?1.0.0.25664';
		                                head.appendChild(link);
		                                return true;
		                            } else {
		                                document.write('<link href="/Content/livetiming-events?1.0.0.25664" rel="stylesheet" type="text/css" />');
		                            }
		                        }
		                    }
		                }
		                return true;
		            }())||document.write('<script src="/Content/livetiming-events"><\/script>');</script>


		  <script src="https://speedhive.azureedge.net/bundles/scripts?1.0.0.25664"></script>
		<script>(window.$)||document.write('<script src="/bundles/scripts"><\/script>');</script>

		  <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,300i,700" rel="stylesheet">
		<style>
			.header {
			    min-height: 152px;
			    padding: 14px 0 0;
			}
		</style>

</head>
	<body>
		<div class="page-container">
			<?php $this->load->view('header');?>
			<div class="body-content" id="js_print">
				<div class="container all-country">
					<div class="row">
				        <div class="col-xs-12">
				        	<div id="events-filter">
					            <ul id="sport-options" class="sport-options clearfix sportOptionsList">
					                <li class="event-tab all-tab active" id="alltab">
					                    <a href="#" data-toggle="tab" data-sport-type="all">All Sports</a>
					                </li>
					            </ul><!-- sport-options -->
					        </div>
				        </div>
				    </div>
				    <div class="row">
				        <div id="event-list" class="col-md-8 col-sm-12 col-xs-12">
				        	<div class="row live-events">
							  <div class="col-xs-12">
							    <div class="row-event-title">
							      <h3 class="title">
							        Live Events
							      </h3>
							    </div>
							    <div>
							    	<?php echo $live_result; ?>
							    </div>
							  </div>
							</div>
							<div id="finished-events" class="row">
							    <div class="col-xs-12">
							        <div class="row-event-title">
							            <h3 class="title">
							                Recently Finished Events
							            </h3>
							        </div>
							        <?php echo $finish_result; ?>
							        <!-- <a class="row-event vertically-center" href="/LiveTiming/SQKVNIBO-2147484314" data-country-code="gb" data-sport="10">
							            <div class="event-flag">
							                <span class="finish-flag"></span>
							            </div>
							            <div class="event-details">
							                <div class="event-name">
							                    57th Eddie Soens Memorial 2018
							                </div>
							                <div class="event-extra">
							                    <div class="event-location">
							                        
							                            Liverpool, United Kingdom,  United Kingdom
							                        
							                    </div>
							                    <div class="event-schedule">
							                        <div class="event-date">
							                            10 Mar 2018
							                        </div>
							                    </div>
							                </div>
							            </div>
							            <div class="event-status">
							                <span class="ico ico-arrow-right-gray"></span>
							            </div>
							        </a> -->
							    </div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>