<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<html>
	<!-- <head> -->
	<head prefix="og:http://ogp.me/ns#">
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">


		<meta name="apple-itunes-app" content="app-id=988412583" app-argument="">
		<meta name="google-play-app" content="app-id=com.mylaps.speedhive">
		<title>Speedhive - All Event Results</title>
		<meta property="og:title" content="Speedhive - All Event Results">
		<meta name="description" content="The platform for racers. MYLAPS Speedhive is your place to check results, follow races and go faster.">
		<meta name="og:description" content="The platform for racers. MYLAPS Speedhive is your place to check results, follow races and go faster.">
		<meta property="og:image" content="https://speedhive.mylaps.com/Images/images/share.jpg">
		<meta name="twitter:card" content="summary_large_image">
		<meta name="twitter:site" content="@mylaps">
		<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
		<link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
		<link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
		<link rel="manifest" href="/manifest.json">
		<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#f7941e">
		<link rel="shortcut icon" href="/favicon.ico">
		<meta name="msapplication-TileColor" content="#f7941e">
		<meta name="msapplication-TileImage" content="/mstile-144x144.png">
		<meta name="msapplication-config" content="/browserconfig.xml">
		<meta name="theme-color" content="#ffffff">

		<link href="https://speedhive.azureedge.net/Content/css?1.0.0.25664" rel="stylesheet">
		<link href="https://speedhive.azureedge.net/Content/flags?1.0.0.25664" rel="stylesheet">
		<script>(function() {
		        var loadFallback,
		            len = document.styleSheets.length;
		        for (var i = 0; i < len; i++) {
		            var sheet = document.styleSheets[i];
		            if (sheet.href.indexOf('https://speedhive.azureedge.net/Content/flags?1.0.0.25664') !== -1) {
		                var meta = document.createElement('meta');
		                meta.className = 'ico-flag-ad';
		                document.head.appendChild(meta);
		                var value = window.getComputedStyle(meta).getPropertyValue('background-position');
		                document.head.removeChild(meta);
		                if (value !== '0 0') {
		                    if(document.readyState === 'complete') {
		                        var head  = document.getElementsByTagName('head')[0];
		                        var link  = document.createElement('link');
		                        link.rel  = 'stylesheet';
		                        link.type = 'text/css';
		                        link.href = '/Content/flags?1.0.0.25664';
		                        head.appendChild(link);
		                        return true;
		                    } else {
		                        document.write('<link href="/Content/flags?1.0.0.25664" rel="stylesheet" type="text/css" />');
		                    }
		                }
		            }
		        }
		        return true;
		    }())||document.write('<script src="/Content/flags"><\/script>');</script>

		<link href="https://speedhive.azureedge.net/Content/event?1.0.0.25664" rel="stylesheet">
		<script>(function() {
		        var loadFallback,
		            len = document.styleSheets.length;
		        for (var i = 0; i < len; i++) {
		            var sheet = document.styleSheets[i];
		            if (sheet.href.indexOf('https://speedhive.azureedge.net/Content/event?1.0.0.25664') !== -1) {
		                var meta = document.createElement('meta');
		                meta.className = 'event-search event-search-term';
		                document.head.appendChild(meta);
		                var value = window.getComputedStyle(meta).getPropertyValue('height');
		                document.head.removeChild(meta);
		                if (value !== '50px') {
		                    if(document.readyState === 'complete') {
		                        var head  = document.getElementsByTagName('head')[0];
		                        var link  = document.createElement('link');
		                        link.rel  = 'stylesheet';
		                        link.type = 'text/css';
		                        link.href = '/Content/event?1.0.0.25664';
		                        head.appendChild(link);
		                        return true;
		                    } else {
		                        document.write('<link href="/Content/event?1.0.0.25664" rel="stylesheet" type="text/css" />');
		                    }
		                }
		            }
		        }
		        return true;
		    }())||document.write('<script src="/Content/event"><\/script>');</script>


		<script src="https://speedhive.azureedge.net/bundles/scripts?1.0.0.25664"></script>
		<script>(window.$)||document.write('<script src="/bundles/scripts"><\/script>');</script>
		<style>
			.header {
			    min-height: 152px;
			    padding: 14px 0 0;
			}
		</style>
	</head>
	<body>
		<div class="page-container">
			<?php $this->load->view('header');?>
			<div class="body-content" id="js_print">
				<div class="container">
					<div class="row">
				        <div class="col-xs-12">
				            <ul id="sport-options" class="sport-options clearfix sportOptionsList">
				                <li class="event-tab all-tab active" id="alltab">
				                    <a href="#" data-toggle="tab" data-sport-type="all">All Sports</a>
				                </li>
				            </ul><!-- sport-options -->
				        </div>
				    </div>
				    <div class="row">
				        <div class="col-md-8 col-sm-12 col-xs-12">
				            <div id="personal"></div>
				            <div class="row">
				            	<div id="event-resultsstop" class="stopdata-loading event-results events-list all-countries col-xs-12">
									<div id="event-rows">
									<?php echo $result; ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>