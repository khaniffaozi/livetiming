<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Events extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct() {
		parent::__construct();
		include APPPATH . 'third_party/simple_html_dom.php';
	}

	public function index()
	{
		$this->load->view('events');
	}

	public function detail($id = "")
	{
		$html = new Simple_html_dom();
		$html->load_file('https://speedhive.mylaps.com/Events/' . $id);
		$lists = $html->find('.event-details');
		$item = '';
		foreach ($lists as $element) {
			$details =  $html->find('#event-details-infostop a');
			foreach ($details as $detail) {
				$href = explode('/',$detail->href);
				$detail->href = base_url('Events/sessions/').$href[2];
			}
		}
		$data['result'] = $element;
		$this->load->view('detail', $data);
	}

	public function sessions($id = "")
	{
		$html = new Simple_html_dom();
		$html->load_file('https://speedhive.mylaps.com/Sessions/' . $id);
		$lists1 = $html->find('#session-header');
		$lists2 = $html->find('.tab-content');

		foreach ($lists1 as $result1) {
			$details =  $html->find('.session-details-link');
			foreach ($details as $detail) {
				$detail->href = "#";
			}
		}
		foreach ($lists2 as $result2) {
			$details =  $html->find('#session-results a');
			foreach ($details as $detail) {
				$detail->href = "#";
			}
		}
		$data['result1'] = $result1;
		$data['result2'] = $result2;
		$this->load->view('sessions', $data);
	}
}
