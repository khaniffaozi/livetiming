<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Livetiming extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct() {
		parent::__construct();
		include APPPATH . 'third_party/simple_html_dom.php';
	}

	public function index()
	{
		$html = new Simple_html_dom();
		$data = array();
		$live_details = '';
		$finish_details = '';
		$html->load_file('https://speedhive.mylaps.com/LiveTiming');

			// echo count($img);die;
		$live_lists = $html->find('.live-events > div > a');
		// echo count($live_lists);die;
		foreach ($live_lists as $key=>$element) {
			// echo $element;
			$img_source =  $html->find('.event-premium',$key);
			if($img_source){
				$img = $img_source->src;
				$img_source->src = 'https://speedhive.mylaps.com' . $img;
			}
			$e = $element->getAllAttributes();
			if($e["data-country-code"] == 'id'){
				$href = explode('/',$element->href);
				$element->href = $href[2];
				$live_details .= $element;
			}
		}
		$data['live_result'] = $live_details;


		$finish_lists = $html->find('#finished-events > a');
		foreach ($finish_lists as $key=>$element) {
			$e = $element->getAllAttributes();
			if($e["data-country-code"] == 'id'){
				$href = explode('/',$element->href);
				$element->href = base_url().$href[2];
				$finish_details .= $element;
			}
			
		}
		$data['finish_result'] = $finish_details;

		$this->load->view('livetiming', $data);
	}
	public function test()
	{
		# code...
		$html = new Simple_html_dom();
		$data = array();
		$live_details = '';
		$finish_details = '';
		$html->load_file('https://speedhive.mylaps.com/LiveTiming');

			// echo count($img);die;
		$live_lists = $html->find('.live-events > div > a');
		// echo count($live_lists);die;
		foreach ($live_lists as $key=>$element) {
			// echo $element;
			$img_source =  $html->find('.event-premium',$key);
			$img = $img_source->src;
			$img_source->src = 'https://speedhive.mylaps.com' . $img;

			$href = explode('/',$element->href);
			$element->href = $href[2];

			$live_details .= $element;
		}
		$data['live_result'] = $live_details;


		$finish_lists = $html->find('#finished-events > a');
		foreach ($finish_lists as $key=>$element) {

			$href = explode('/',$element->href);
			$element->href = base_url().$href[2];

			$finish_details .= $element;
		}
		$data['finish_result'] = $finish_details;

		$this->load->view('test', $data);
	}
}
